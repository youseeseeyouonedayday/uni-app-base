/**
 * 新版本的 husky 这个配置文件基本无用了
 * 1. npx husky install
 * 2. npx husky add .husky/pre-commit "npx lint-staged"
 */
module.exports = {
  hooks: {
    'pre-commit': 'npx lint-staged',
  },
}
