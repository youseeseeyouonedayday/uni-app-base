module.exports = {
  tabWidth: 2,
  endOfLine: 'auto',
  useTabs: false,
  printWidth: 120,
  singleQuote: true,
  semi: false,
}
