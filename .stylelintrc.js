module.exports = {
  root: true,
  extends: [
    'stylelint-config-recess-order',
    'stylelint-config-standard-scss',
    'stylelint-config-html/vue',
    'stylelint-config-prettier'
  ],
  overrides: [],
  rules: {
    'unit-no-unknown': [
      true,
      {
        ignoreUnits: ['rpx'],
      },
    ],
  },
  'selector-type-no-unknown': [
    true,
    {
      ignoreTypes: ['page', 'radio', 'uni-radio', 'cover-image', 'cover-view'],
    },
  ],
  'selector-pseudo-class-no-unknown': [
    true,
    {
      ignorePseudoClasses: ['global'],
    },
  ],
}


